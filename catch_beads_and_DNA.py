"""Catch beads and DNA, then exit."""

import bluelake as bl
import parameters as pm
import numpy as np
import autotrap.catch as autocatch
import importlib
import time
import shutil
import os

importlib.reload(autocatch)
importlib.reload(pm)


def bead_DNA_catch():

    # x, y = bl.mirror2.position
    # bl.mirror1.move_to(x=x+pm.DNA_FISHING_PARAMS['max_distance_fishing'], y=y)

    # Catch beads, fish for DNA, and check against the WLC.
    autocatch.catch_beads(match_threshold=pm.BEAD_CATCHING_PARAMS['bead_match_threshold'],
                          verbose=pm.IO_PARAMS['verbose'],
                          pressure=pm.SETUP_PARAMS['flow_pressure'])
    autocatch.catch_dna(min_distance=pm.DNA_FISHING_PARAMS['min_distance_fishing'],
                        max_distance=pm.DNA_FISHING_PARAMS['max_distance_fishing'],
                        fishing_speed=pm.DNA_FISHING_PARAMS['dna_fishing_speed'],
                        match_threshold=pm.BEAD_CATCHING_PARAMS['bead_match_threshold'],
                        end_distance=pm.DNA_FISHING_PARAMS['end_distance_fishing'],
                        force_threshold=pm.DNA_FISHING_PARAMS['force_threshold'])
    autocatch.check_dna(match_threshold=pm.BEAD_CATCHING_PARAMS['bead_match_threshold'],
                        dna_contour_length=pm.DNA_PARAMS['Lc_ds'],
                        distance_threshold=pm.DNA_FISHING_PARAMS['distance_threshold'],
                        tether_lost_threshold=pm.DNA_FISHING_PARAMS['tether_lost_threshold'],
                        force_check=pm.DNA_FISHING_PARAMS['force_check'],
                        fclamp_par=pm.FORCE_CLAMP_PARAMS,
                        end_distance=pm.DNA_FISHING_PARAMS['end_distance_checking'])
    autocatch.straighten_dna()
    print('Bead and DNA catching has finished.')


# This command runs the workflow.
bead_DNA_catch()
