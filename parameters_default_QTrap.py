import lumicks.pylake as lk

"""Input/output"""
IO_PARAMS = {
    'experiment_name': 'example',       # Experiment name
    'output_path': 'D:/ZL/20220124',    # Output path for h5 data
    'verbose': False                    # Print bead locations and match scores to bluelake terminal
}

"""Experiment setup"""
SETUP_PARAMS = {
    'setup': 'QTrap',                   # Either 'CTrap' or 'QTrap'
    'max_repeats': 10,                  # Number of repeats for experiment
    'name_bead_channel': "beads",       # This has to match the bead channel waypoint name in the UI
    'name_dna_channel': "DNA",          # This has to match the dna channel waypoint name in the UI
    'name_buffer_channel': "buffer",    # This has to match the buffer channel waypoint name in the UI
    'time_to_wait_for_flow': 3.0,       # Time to wait for flow to start/end [s]
    'flow_channels': (1, 2, 3)          # Numbers associated with flow channels to turn on and off during experiment
}

"""Bead catching"""
BEAD_CATCHING_PARAMS = {
    'left_bead': 2,                     # Number associated with left bead
    'time_to_wait_for_beads': 30.0,     # Time to wait until resetting the bead catching code [s]
    'bead_match_threshold': 85,         # Minimal template match threshold
    'bead_separator_location': 53.0     # Coordinate between left and right bead [um]
}

"""DNA fishing"""
DNA_FISHING_PARAMS = {
    'dna_fishing_speed': 10,            # DNA fishing speed [unit?]
    'min_distance_fishing': 2.5,        # Minimal distance when fishing for DNA [um]
    'max_distance_fishing': 8.5,        # Maximal distance when fishing for DNA [um]
    'force_threshold': 8,               # Force threshold when fishing for DNA [pN]
    'force_check': 8,                   # Check WLC at this force
    'distance_threshold': 1.3,          # Allowed distance error at WLC check
    'tether_lost_threshold': 5.0,       # If force falls below this threshold at WLC check, tether is assumed lost [pN]
    'end_distance_fishing': 4.0         # Go to this distance after fishing, and reset force [um]
}

"""FD curve parameters"""
FD_CURVE_PARAMS = {
    'min_distance_fd': 4,               # Starting distance of F,d curve [um]
    'max_distance_fd': 9,               # (Approximate) ending distance of F,d curve [um]
    'fd_speed': 0.5,                    # Speed at which to pull [unit?]
    'replicates': 2                     # Maximal number of replicates per tether (assuming it isn't lost) [#]
}

"""Confocal scan parameters"""
CONFOCAL_PARAMS = {
    'force_confocal': 2,                # Force at which to take confocal scan [pN]
    'intensity_threshold': 0.15,        # If the mean projected intensity goes under this value, stop the scan [ADU]
    'image_time': 0.6,                  # Time between frames; set to same number as you set in GUI
    'margin_bead_px': (30, 40),         # Left and right margins for beads in the confocal scan [px]
    'max_t': 120.,                      # Maximum scan length [s]
    'check_interval': 5.,               # Time interval for bleaching check [s]
    'save_figs': True                   # Save confocal bleaching analysis figures
}

"""DNA substrate parameters"""
nt_DNA = 26000
DNA_PARAMS = {
    'nt_DNA': nt_DNA,                   # total length of DNA in basepairs
    'Lp_ds': 53,                        # persistence length (nm)
    'Lc_ds': nt_DNA * 0.34 * 0.001,     # contour length double stranded (um)
    'kT': 4.11,                         # Boltzmann*temperature, default is 4.11 pN nm
    'S': 1533,                          # stretching modulus, default is 1500
    'model_ewlc': lk.odijk("model")     # eWLC model
}

"""Force clamp parameters"""
FORCE_CLAMP_PARAMS = {
    'device': "1",
    'detector': "Trap 2",
    'frequency': 500,
    'kp': 0.5,
    'ki': 0.0,
    'kd': 0.0,
    'reversed': False,
    'angle': 0,
    'max_step': 50.
}
