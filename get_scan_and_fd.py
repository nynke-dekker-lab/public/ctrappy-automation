"""Catch beads and DNA, take confocal scan, make fd curve."""

import bluelake as bl
import parameters as pm
import numpy as np
import autotrap.catch as autocatch
import autotrap.confocal as autoconf
import autotrap.fd as autofd
import importlib
import time
import shutil
import os

importlib.reload(autocatch)
importlib.reload(autoconf)
importlib.reload(autofd)
importlib.reload(pm)


def fd_workflow(experiment_name, path, verbose):

    path = autocatch.validate_dir(path)
    experiment_t = str(int(time.time()))
    # Copy parameter file with experiment name and timestamp.
    shutil.copyfile(os.path.join(pm.IO_PARAMS['code_dir'], 'parameters.py'),
                    os.path.join(path, experiment_t + '_' + experiment_name + '_parameters_Copy.py'))

    tether_count = -1
    while tether_count < pm.SETUP_PARAMS['max_repeats']:
        try:
            tether_count += 1

            # Catch beads, fish for DNA, and check against the WLC.
            autocatch.catch_beads(match_threshold=pm.BEAD_CATCHING_PARAMS['bead_match_threshold'],
                                  verbose=verbose, pressure=pm.SETUP_PARAMS['flow_pressure'])
            autocatch.catch_dna(min_distance=pm.DNA_FISHING_PARAMS['min_distance_fishing'],
                                max_distance=pm.DNA_FISHING_PARAMS['max_distance_fishing'],
                                fishing_speed=pm.DNA_FISHING_PARAMS['dna_fishing_speed'],
                                match_threshold=pm.BEAD_CATCHING_PARAMS['bead_match_threshold'],
                                end_distance=pm.DNA_FISHING_PARAMS['end_distance_fishing'],
                                force_threshold=pm.DNA_FISHING_PARAMS['force_threshold'])
            autocatch.check_dna(match_threshold=pm.BEAD_CATCHING_PARAMS['bead_match_threshold'],
                                dna_contour_length=pm.DNA_PARAMS['Lc_ds'],
                                distance_threshold=pm.DNA_FISHING_PARAMS['distance_threshold'],
                                tether_lost_threshold=pm.DNA_FISHING_PARAMS['tether_lost_threshold'],
                                force_check=pm.DNA_FISHING_PARAMS['force_check'],
                                fclamp_par=pm.FORCE_CLAMP_PARAMS,
                                end_distance=pm.DNA_FISHING_PARAMS['end_distance_checking'])
            autocatch.straighten_dna()

            # Take confocal scan at force_confocal.
            name = f"{experiment_t + '_' + experiment_name}_tether={tether_count:04}"
            autocatch.goto_force(pm.FORCE_CLAMP_PARAMS, pm.CONFOCAL_PARAMS['force_confocal'],
                                 pm.BEAD_CATCHING_PARAMS['bead_match_threshold'],
                                 pm.CONFOCAL_PARAMS['force_confocal']/2, pm.DNA_PARAMS['Lc_ds'])
            autoconf.take_confocal_scan(experiment_name=name, path=path,
                                        intensity_threshold=pm.CONFOCAL_PARAMS['intensity_threshold'],
                                        image_time=pm.CONFOCAL_PARAMS['image_time'],
                                        margin_bead_px=pm.CONFOCAL_PARAMS['margin_bead_px'],
                                        max_t=pm.CONFOCAL_PARAMS['max_t'],
                                        check_interval=pm.CONFOCAL_PARAMS['check_interval'],
                                        save_figs=pm.CONFOCAL_PARAMS['save_figs'])

            # Take fd curves.
            for replicate in np.arange(pm.FD_CURVE_PARAMS['replicates']):
                print(f"Recording F,d curve (tether: {tether_count}, replicate: {replicate}).")
                name = f"{experiment_t + '_' + experiment_name}_tether={tether_count:04}_replicate={replicate:04}"
                if not pm.FD_CURVE_PARAMS['max_use_force']:
                    autofd.make_fd_curve(min_distance=pm.FD_CURVE_PARAMS['min_distance_fd'],
                                         max_distance=pm.FD_CURVE_PARAMS['max_distance_fd'],
                                         force_threshold=pm.DNA_FISHING_PARAMS['force_threshold'],
                                         match_threshold=pm.BEAD_CATCHING_PARAMS['bead_match_threshold'],
                                         fd_speed=pm.FD_CURVE_PARAMS['fd_speed'],
                                         loop=pm.FD_CURVE_PARAMS['loop'],
                                         name=name, path=path)
                else:
                    autofd.make_fd_curve_max_force(min_distance=pm.FD_CURVE_PARAMS['min_distance_fd'],
                                                   max_force=pm.FD_CURVE_PARAMS['max_force_fd'],
                                                   force_threshold=pm.DNA_FISHING_PARAMS['force_threshold'],
                                                   match_threshold=pm.BEAD_CATCHING_PARAMS['bead_match_threshold'],
                                                   fclamp_par=pm.FORCE_CLAMP_PARAMS,
                                                   dna_contour_length=pm.DNA_PARAMS['Lc_ds'],
                                                   fd_speed=pm.FD_CURVE_PARAMS['fd_speed'],
                                                   loop=pm.FD_CURVE_PARAMS['loop'],
                                                   name=name, path=path,
                                                   spring_constant=pm.FD_CURVE_PARAMS['spring_constant'],
                                                   pause=pm.FD_CURVE_PARAMS['pause'])

        except RuntimeError as e:
            print(e)
            print("Restarting protocol.")

        finally:
            bl.shutters.clear(1)
            bl.shutters.clear(2)


# This command runs the workflow.
fd_workflow(experiment_name=pm.IO_PARAMS['experiment_name'],
            path=pm.IO_PARAMS['output_path'],
            verbose=pm.IO_PARAMS['verbose'])
