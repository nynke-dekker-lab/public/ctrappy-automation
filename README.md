# CTrap automation

This is a collection of scripts that can be run within bluelake for 
C-/Q-trap automation.

## Usage

- Parameters can be set in the file `parameters.py`.
- A copy of default parameters can be found in `parameters_default_*.py`.
- To run a script in Bluelake, press the `</>` button and open one of the following files:
    - To take FD curves, run `get_fd_curve.py`.
    - To take confocal scans, run `get_confocal_scan.py`.
    - To take both confocal scans and FD curves, run `get_scan_and_fd.py`.
- It is advised to save a copy of the `parameters.py` file with the output data for future reference.

## First run

In a first run (or a run after the setup has been serviced), the user needs to set a 
few parameters. Most importantly:
- The 'bead separator location' for distinguishing the left bead from the right bead
- The 'bead margins' for cutting out the beads from the image during confocal scanning
- The 'intensity threshold' for checking if the signal has bleached

The first parameter can be set by setting `IO_PARAMS['verbose']` to `True`.
The script then shows absolute coordinates of detected beads while it is running, 
which the user can use to find a coordinate inbetween the left and the right bead.

The last two parameters can be set by setting `CONFOCAL_PARAMS['save_figs']` to `True`.
Then, during a confocal scan, the script will output confocal frames with intensity
projections, which the user can use to select an intensity threshold as well as
distances (in px) from the left and right image edge to crop out, to get rid of the
signal comming off the beads.

Of course, other experimental parameters also need to be set, such as the correct
channels to move to, the min and max distance while DNA fishing, et cetera.

## Workflow

In each of the aforementioned scripts, the workflow is as follows:
- Start flow, move to bead channel and catch beads
- Move to DNA channel, catch DNA, straighten DNA
- Stop flow, move to buffer channel
- In buffer channel, take confocal scan and/or FD curves

There are some scripts (`check_fluorescence_before_timelapse.py` for example)
where the workflow is a little more complicated.

## Code structure

The core code contains three sub-modules: one for catching beads and DNA 
(autotrap.catch), one for doing confocal scanning (autotrap.confocal), 
and one for taking force-distance curves (autotrap.fd). The scripts in the repo
root call upon these functions to perform automatic experiments. The functions
are documented in docstrings (in the functions themselves); for the most important
functions each input argument is specified.

## Parameters

**`IO_PARAMS`**: Parameters for input and output
- `code_dir`: # Code location
- `experiment_name`: Experiment name, is used for filenames during output
- `output_path`: Output path; make sure this folder exists
- `verbose`: Set to True to print bead locations and match scores to the bluelake terminal;
these locations and scores can be used to set the bead catching parameters [bool]

**`SETUP_PARAMS`**: Basic experiment setup
- `setup`: Either 'CTrap' or 'QTrap'
- `max_repeats`: Number of repeats for experiment [#]
- `name_bead_channel`: This has to match the bead channel waypoint name in the UI
- `name_dna_channel`: This has to match the dna channel waypoint name in the UI
- `name_buffer_channel`: This has to match the buffer channel waypoint name in the UI
- `time_to_wait_for_flow`: Time to wait for flow to start/end [s]
- `flow_channels`: Numbers associated with flow channels to turn on and off during experiment
- `flow_pressure`: Pressure in flow channels [bar]

**`BEAD_CATCHING_PARAMS`**: Bead catching parameters
- `left_bead`: Number associated with left bead
- `time_to_wait_for_beads`: Time to wait until resetting the bead catching code [s]
- `bead_match_threshold`: Minimal template match threshold [%]
- `bead_separator_location`: Coordinate between left and right bead [um]

**`DNA_FISHING_PARAMS`**: DNA fishing parameters
- `dna_fishing_speed`: DNA fishing speed [um/s]
- `min_distance_fishing`: Minimal distance when fishing for DNA [um]
- `max_distance_fishing`: Maximal distance when fishing for DNA [um]
- `force_threshold`: Force threshold when fishing for DNA [pN]
- `force_check`: Check WLC at this force [pN]
- `distance_threshold`: Allowed distance error at WLC check [um]
- `tether_lost_threshold`: If force falls below this threshold at WLC check, tether is assumed lost [pN]
- `end_distance_fishing`: Go to this distance after fishing, and reset force [um]
- `end_distance_checking`: Go to this distance after checking the DNA against the WLC [um]

**`FD_CURVE_PARAMS`**: FD curve parameters
- `min_distance_fd`: Starting distance of F,d curve [um]
- `max_distance_fd`: (Approximate) ending distance of F,d curve [um]
- `max_use_force`: Set to True to override max_distance_fd with a force value [bool]
- `max_force_fd`: Ending force of F,d curve [pN]
- `loop`: Set to True to loop back to min_distance_fd after reaching max_*_fd [bool]
- `fd_speed`: Speed at which to pull [um/s]
- `replicates`: Maximal number of replicates per tether (assuming it isn't lost) [#]

**`CONFOCAL_PARAMS`**: Confocal scan parameters
- `force_confocal`: Force at which to take confocal scan [pN]
- `intensity_threshold`: If the mean projected intensity goes under this value, stop the scan [ADU]
- `image_time`: Time between frames; set to same number as you set in GUI
- `margin_bead_px`: Left and right margins for beads in the confocal scan [px]
- `max_t`: Maximum scan length [s]
- `check_interval`: Time interval for bleaching check [s]
- `save_figs`: Save confocal bleaching analysis figures

**`DNA_PARAMS`**: DNA substrate parameters
- `nt_DNA`: Total length of DNA [basepairs]
- `Lp_ds`: Persistence length [nm]
- `Lc_ds`: Contour length double stranded [um]
- `kT`: Boltzmann constant * temperature, default is 4.11 [pN nm]
- `S`: Stretching modulus, default is 1500
- `model_ewlc`: eWLC model to use

**`FORCE_CLAMP_PARAMS`**: Force clamp parameters
- `device`: See bluelake GUI
- `detector`: See bluelake GUI
- `frequency`: See bluelake GUI
- `kp`: See bluelake GUI
- `ki`: See bluelake GUI
- `kd`: See bluelake GUI
- `reversed`: See bluelake GUI
- `angle`: See bluelake GUI
- `max_step`: See bluelake GUI

## Authors

- Edo van Veen: *initial code development*
- Zhaowei Liu: *testing and development of base functionality, current code maintainer*
- Humberto Sanchez: *developing and testing experiment workflows*

Nynke Dekker Lab, TU Delft, 2022

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
