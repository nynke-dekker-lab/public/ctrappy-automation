import lumicks.pylake as lk

"""Input/output"""
IO_PARAMS = {
    'code_dir': 'Z:/Automation',        # Code location
    'experiment_name': 'chromatin_Lig-H2A_MUT_ORC_tube',     # Experiment name
    'output_path': 'D:/HS/221110',      # Output path for h5 data
    'verbose': False                    # Print  bead locations and match scores to bluelake terminal
}

"""Experiment setup"""
SETUP_PARAMS = {
    'setup': 'QTrap',                   # Either 'CTrap' or 'QTrap'
    'max_repeats': 200,                  # Number of repeats for experiment
    'name_bead_channel': "Waypoint (10)",       # This has to match the bead channel waypoint name in the UI Waypoint (10)
    'name_dna_channel': "Waypoint (11)",        # This has to match the dna channel waypoint name in the UI
    'name_buffer_channel': "Waypoint (38)",     # This has to match the buffer channel waypoint name in the UI
    'time_to_wait_for_flow': 3.0,       # Time to wait for flow to start/end [s]
    'flow_channels': (1, 2, 3),         # Numbers associated with flow channels to turn on and off during experiment
    'flow_pressure': 0.15
                    # Pressure in flow channels
}

"""Bead catching""" 
BEAD_CATCHING_PARAMS = {
    'left_bead': 2,                     # Number associated with left bead
    'time_to_wait_for_beads': 30.0,     # Time to wait until resetting the bead catching code [s]
    'bead_match_threshold': 85,         # Minimal template match threshold
    'bead_separator_location': 53.0     # Coordinate between left and right bead [um]
}

"""DNA fishing"""
DNA_FISHING_PARAMS = {
    'dna_fishing_speed': 5,            # DNA fishing speed [um/s]
    'min_distance_fishing': 2,        # Minimal distance when fishing for DNA [um] 1.5 or 2
    'max_distance_fishing': 3.7,        # Maximal distance when fishing for DNA [um]
    'force_threshold': 5,               # Force threshold when fishing for DNA [pN]
    'force_check': 2,                   # Check WLC at this force
    'distance_threshold': 0.5,          # Allowed distance error at WLC check [um] 1.3 or 10% from Lc,
    'tether_lost_threshold': 1.0,       # If force falls below this threshold at WLC check, tether is assumed lost [pN]
    'end_distance_fishing': 1.5,        # Go to this distance after fishing, and reset force [um]
    'end_distance_checking': 2.5        # Go to this distance after checking against the WLC [um]
}

"""FD curve parameters"""
FD_CURVE_PARAMS = {
    'min_distance_fd': 1.5,             # Starting distance of F,d curve [um]
    'max_distance_fd': 4 ,              # (Approximate) ending distance of F,d curve [um]
    'max_use_force': True,              # Override max_distance_fd with a force value
    'max_force_fd': 55,                 # Ending force of F,d curve [pN]
    'loop': True,                       # Loop back to min_distance_fd after reaching max_distance_fd (True/False)
    'fd_speed': 0.1,                    # Speed at which to pull [um/s]
    'replicates': 1,                    # Maximal number of replicates per tether (assuming it isn't lost) [#]
    'spring_constant': 0.4,          # Estimated spring cons tant [pN/nm] times 2
    'pause': 0                          # If 'max_use_force'==True, pause at max force for this amount of time [s]
}

"""Confocal scan parameters"""
CONFOCAL_PARAMS = {
    'force_confocal': 2,                # Force at which to take confocal scan [pN]
    'intensity_threshold': (0.5, 0.5, 0.15),       # (R,G,B) If the mean projected intensity goes under this value, stop the scan [ADU]0.15,
    'image_time': 0.6,                  # Time between frames; set to same number as you set in GUI
    'margin_bead_px': (25, 25),         # A 2-tuple with L and R margins for beads in the confocal scan [px]
    'max_t': 600.,                      # Maximum scan length [s]120.,400 or 600 for ORC in chromatin
    'check_interval': 10.,               # Time interval for bleaching check [s]
    'save_figs': False                  # Save confocal bleaching analysis figures
}

"""DNA substrate parameters"""
nt_DNA = 10439
DNA_PARAMS = {
    'nt_DNA': nt_DNA,                   # total length of DNA in basepairs
    'Lp_ds': 53,                        # persistence length (nm)
    'Lc_ds': nt_DNA * 0.34 * 0.001,     # contour length double stranded (um)
    'kT': 4.11,                         # Boltzmann*temperature, default is 4.11 pN nm
    'S': 1533,                          # stretching modulus, default is 1500
    'model_ewlc': lk.odijk("model")     # eWLC model
}

"""Force clamp parameters"""
FORCE_CLAMP_PARAMS = {
    'device': "1",
    'detector': "Trap 2",
    'frequency': 500,  # [Hz]
    'kp': 0.5,
    'ki': 0.0,
    'kd': 0.0,
    'reversed': False,
    'angle': 0,
    'max_step': 50.  # [nm]
}
