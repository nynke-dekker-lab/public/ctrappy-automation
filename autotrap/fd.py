"""Functions for force-distance curve measurement."""
import bluelake as bl
from autotrap.catch import goto_distance, get_force, goto_force, get_distance
import copy


def make_fd_curve(min_distance, max_distance, force_threshold, match_threshold, fd_speed, loop, name, path):
    """Measure an Fd curve using a start and end distance; save to file.

    Parameters
    ----------
    min_distance : float
        Start at this bead distance (micron).
    max_distance : float
        End at this bead distance (micron).
    force_threshold : float
        If force goes under this threshold (pN), the tether is considered lost.
    match_threshold : float
        If the match score goes under this threshold (percent), the bead is considered lost.
    fd_speed : float
        Speed of distance increase in um/s.
    loop : bool
        If True, loop back to min_distance after reaching max_distnace.
    name : str
        Filename for saving h5 file.
    path : str
        Path to save the file to.
    """
    goto_distance(min_distance, match_threshold, speed=5)
    bl.reset_force()
    bl.pause(0.4)

    try:
        bl.timeline.mark_begin(name)
        bl.mirror1.move_by(dx=max_distance - min_distance, speed=fd_speed)
        if loop:
            bl.pause(0.05)
            bl.mirror1.move_by(dx=min_distance - max_distance, speed=fd_speed)
        bl.pause(0.05)
    finally:
        bl.timeline.mark_end(export=True if path else False, filepath=f"{path}/{name}.h5")

    if get_force() < force_threshold:
        raise RuntimeError(f"Lost tether.")

    bl.pause(1)


def make_fd_curve_max_force(min_distance, max_force, force_threshold, match_threshold, fclamp_par,
                            dna_contour_length, fd_speed, loop, name, path, spring_constant=0, pause=0.05):
    """Measure an Fd curve using a start distance and end force; save to file.

    Parameters
    ----------
    min_distance : float
        Start at this bead distance (micron).
    max_force : float
        End at this force (pN).
    force_threshold : float
        If force goes under this threshold (pN), the tether is considered lost.
    match_threshold : float
        If the match score goes under this threshold (percent), the bead is considered lost.
    fclamp_par : dict
        Dictionary with force clamp parameters.
    dna_contour_length : float
        DNA contour length in microns.
    fd_speed : float
        Speed of distance increase in um/s.
    loop : bool
        If True, loop back to min_distance after reaching max_distnace.
    name : str
        Filename for saving h5 file.
    path : str
        Path to save the file to.
    spring_constant : float
        Spring constant in pN/nm.
    pause : float
        Wait this many seconds before looping back, if looping.
    """

    goto_distance(min_distance, match_threshold, speed=5)
    bl.reset_force()
    bl.pause(0.4)
    bl.timeline.mark_begin(name)

    try:
        fclamp = copy.deepcopy(fclamp_par)
        fclamp['max_step'] = fd_speed / fclamp['frequency'] * 1000

        goto_force(fclamp, max_force, match_threshold, force_threshold, dna_contour_length)
        if loop:
            bl.pause(pause)
            if spring_constant > 0:
                d_correction = 0.001 * max_force / spring_constant
            else:
                d_correction = 0
            bl.mirror1.move_by(dx=min_distance - get_distance() - d_correction, speed=fd_speed)
    finally:
        bl.timeline.mark_end(export=True if path else False, filepath=f"{path}/{name}.h5")

    if get_force() < force_threshold:
        raise RuntimeError(f"Lost tether.")

    bl.pause(1)
