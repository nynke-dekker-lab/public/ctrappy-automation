"""Parts of this file were adapted from a LUMICKS script, containing the following notice:
----------------------------
Copyright 2020, LUMICKS B.V.

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this 
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, 
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import bluelake as bl
import numpy as np
import time
import os
from parameters import *


# Timeline imports.
distance = bl.timeline["Distance"]["Distance 1"]
force = bl.timeline["Force LF"]["Trap 2"]
match_score1 = bl.timeline["Tracking Match Score"]["Bead 1"]
match_loc1 = bl.timeline['Bead position']['Bead 1 X']
match_loc1y = bl.timeline['Bead position']['Bead 1 Y']
match_score2 = bl.timeline["Tracking Match Score"]["Bead 2"]
match_loc2 = bl.timeline['Bead position']['Bead 2 X']
match_loc2y = bl.timeline['Bead position']['Bead 2 Y']


def config_force_feedback(fclamp_dict, target_force):
    """Configure the force feedback; fclamp_dict is a dictionary with parameters, target_force is the force
    to clamp to (float)."""
    bl.force_feedback.enabled = False
    bl.force_feedback.set_device(fclamp_dict["device"])
    bl.force_feedback.set_detector(fclamp_dict["detector"])
    bl.force_feedback.set_frequency(fclamp_dict["frequency"])
    bl.force_feedback.set_pid_settings(kp=fclamp_dict["kp"], ki=fclamp_dict["ki"], kd=fclamp_dict["kd"],
                                       max_step=fclamp_dict['max_step'], reversed=fclamp_dict["reversed"])
    bl.force_feedback.set_target(target_force)
    bl.force_feedback.set_angle(fclamp_dict["angle"])
    bl.force_feedback.set_lock_motion_angle()


def get_distance():
    """Get current distance between the tracked particles in trap 1 and 2."""
    t0 = bl.timeline.current_time
    bl.pause(0.1)
    t1 = bl.timeline.current_time
    d = np.mean(distance[t0:t1].data)

    return d


def get_force():
    """Get current force between the beads."""
    t0 = bl.timeline.current_time
    bl.pause(0.1)
    t1 = bl.timeline.current_time
    f = np.mean(force[t0:t1].data)

    return f


def throw_if_beads_lost(match_threshold):
    """Raise an exception if we lose the beads, i.e., if the match score drops below match_threshold (float)."""
    if match_score1.latest_value < match_threshold or match_score2.latest_value < match_threshold:
        bl.force_feedback.enabled = False
        raise RuntimeError("Lost beads")


def throw_if_tether_lost(tether_lost_threshold, dna_contour_length):
    """Raise an exception if we lose the tether, i.e., if the force is under the tether_lost_threshold, while
    the distance is more than 1.05 * dna_contour_length."""
    if get_force() < tether_lost_threshold and get_distance() > 1.05 * dna_contour_length:
        bl.force_feedback.enabled = False
        raise RuntimeError("Lost tether.")


def goto_force(fclamp_par, target_force, match_threshold, tether_lost_threshold, dna_contour_length):
    """Move trap 1 until a target force is reached on a tether using force feedback.
    Note: This throws an error if the beads are lost or tether breaks.

    Parameters
    ----------
    fclamp_par : dict
        Dictionary with force clamp parameters.
    target_force : float
        Target force to move to.
    match_threshold : float
        Match threshold for beads (percent); if the match score goes under this threshold, the bead is considered lost.
    tether_lost_threshold : float
        Force threshold for tether in pN; if the force goes under this value, but the DNA length is larger than
        1.05 times dna_contour_length, the tether is considered lost.
    dna_contour_length : float
        DNA contour length in microns.
    """

    # Use the force clamp.
    config_force_feedback(fclamp_par, target_force)
    bl.force_feedback.enabled = True

    # Monitor if tether or beads are lost until we are within 0.5 pN of the target force.
    print("Moving to force:" + str(target_force))
    while abs(get_force() - target_force) > 0.5:
        throw_if_tether_lost(tether_lost_threshold, dna_contour_length)
        throw_if_beads_lost(match_threshold)
    bl.force_feedback.enabled = False


def set_pressure(target):
    """Increase pressure until we are above a certain target (float) level."""
    while bl.fluidics.pressure > target:
        bl.fluidics.decrease_pressure()
        bl.pause(0.5)
    while bl.fluidics.pressure < target:
        bl.fluidics.increase_pressure()
        bl.pause(0.5)


def start_flow(pressure):
    """Start the flow at a certain pressure (float)."""
    print("Starting Flow.")
    set_pressure(pressure)
    bl.fluidics.open(*SETUP_PARAMS['flow_channels'])


def stop_flow():
    """Stop the flow."""
    print("Stopping flow.")
    bl.fluidics.close(*SETUP_PARAMS['flow_channels'])


def release_faulty_bead(match_loc):
    """Release a faulty bead at x-location match_loc (float). Which trap is cleared depends on BEAD_CATCHING_PARAMS,
    elements 'bead_separator_location' and 'left_bead'."""
    print("Releasing faulty bead at %.2f" % match_loc)
    if match_loc < BEAD_CATCHING_PARAMS['bead_separator_location']:  # We are looking at the left bead.
        if BEAD_CATCHING_PARAMS['left_bead'] == 1:
            bl.shutters.clear(1)
        else:
            bl.shutters.clear(2)
    if match_loc >= BEAD_CATCHING_PARAMS['bead_separator_location']:  # We are looking at the right bead.
        if BEAD_CATCHING_PARAMS['left_bead'] == 1:
            bl.shutters.clear(2)
        else:
            bl.shutters.clear(1)


def catch_beads(match_threshold, verbose=True, pressure=0.1):
    """Starts the flow and attempts to catch beads. Toggles the shutters when match score falls below match_threshold
    (float). The parameter verbose (bool) prints live match scores and bead locations if set to True. The pressure
    (float) can also be set."""

    # flow
    start_flow(pressure)

    print(f"Waiting {SETUP_PARAMS['time_to_wait_for_flow']} sec for flow to begin.")
    bl.pause(SETUP_PARAMS['time_to_wait_for_flow'])

    print("Moving to bead channel.")
    bl.microstage.move_to(SETUP_PARAMS['name_bead_channel'])

    bl.pause(1.0)
    bl.shutters.clear(1)
    bl.shutters.clear(2)

    print("Trapping beads.")
    start_time = time.time()
    while match_score1.latest_value < match_threshold or match_score2.latest_value < match_threshold:

        if verbose:
            print("t=%.2f, p1=%.2f, x1=%.2f, p2=%.2f, x2=%.2f" % (time.time() - start_time, match_score1.latest_value,
                  match_loc1.latest_value, match_score2.latest_value, match_loc2.latest_value))

        """Drop beads that do not fulfill the template"""
        if 20.0 < match_score1.latest_value < match_threshold:
            release_faulty_bead(match_loc1.latest_value)
        if 20.0 < match_score2.latest_value < match_threshold:
            release_faulty_bead(match_loc2.latest_value)

        """If it's taking too long, maybe something is stuck in the trap. Clear both traps."""
        if time.time() - start_time > BEAD_CATCHING_PARAMS['time_to_wait_for_beads']:
            print("Clearing shutters...")
            bl.shutters.clear(1)
            bl.shutters.clear(2)
            start_time = time.time()

        bl.pause(0.2)

    print("Got beads!")
    if verbose:
        print("p1=%.2f, x1=%.2f, p2=%.2f, x2=%.2f" % (match_score1.latest_value,
              match_loc1.latest_value, match_score2.latest_value, match_loc2.latest_value))


def straighten_dna(match_threshold=BEAD_CATCHING_PARAMS['bead_match_threshold'], tolerance=0.01, speed=0.1):
    """Straighten the DNA by moving mirror 1. You can set the threshold for the match score (match_threshold, float),
    the location tolerance in the y-direction (tolerance (micron), float) and the speed with which to move the traps
    (speed (um/s), float)."""
    print('Straightening dna.')
    dy = match_loc1y.latest_value - match_loc2y.latest_value
    throw_if_beads_lost(match_threshold)

    while abs(dy) > tolerance:
        if dy < 0:
            bl.mirror1.move_by(dy=-0.005, speed=speed)
        else:
            bl.mirror1.move_by(dy=+0.005, speed=speed)
        dy = match_loc1y.latest_value - match_loc2y.latest_value
        throw_if_beads_lost(match_threshold)
    return True


def goto_distance(target, match_threshold=BEAD_CATCHING_PARAMS['bead_match_threshold'], speed=1,
                  tolerance=0.1, force_threshold=None):
    """Move trap 1 until it reaches the `target` distance from trap 2 (micron).
    Note: This throws an error if the beads are lost (since we will not have a reliable distance then either).

    Parameters
    ----------
    target : float
        Target distance in micron.
    match_threshold : float
        Match threshold for beads (percent); if the match score goes under this threshold, the bead is considered lost.
    speed : float
        Speed with which to move traps in um/s.
    tolerance : float
        Distance tolerance (micron). Default: 0.1.
    force_threshold : float
        Force threshold; if the force goes over this threshold, the function stops and resturns False. Default: None.
    """
    dx = target - distance.latest_value
    throw_if_beads_lost(match_threshold)

    while abs(dx) > tolerance:  # um
        # print(dx, tolerance, speed)
        if dx > 0:
            bl.mirror1.move_by(dx=+0.1, speed=speed)
        else:
            bl.mirror1.move_by(dx=-0.1, speed=speed)

        dx = target - distance.latest_value
        throw_if_beads_lost(match_threshold)
        if force_threshold is not None:
            if get_force() > force_threshold:
                return False
    return True


def catch_dna(min_distance, max_distance, match_threshold, force_threshold,
              fishing_speed, end_distance, max_retries=10):
    """Moves to the DNA channel and starts oscillating the trap until a prescribed force threshold is reached.

    Parameters
    ----------
    min_distance : float
        Minimum distance while fishing.
    max_distance : float
        Maximum distance while fishing.
    match_threshold : float
        Match threshold for beads (percent); if the match score goes under this threshold, the bead is considered lost.
    force_threshold : float
        Force threshold; if the force goes over this threshold, we have caught some DNA.
    fishing_speed : float
        Speed with which to move traps in um/s.
    end_distance : float
        Distance between beads to move to after catching DNA.
    max_retries : int
        Maximum number of tries.
    """
    bl.pause(0.1)

    print("Moving to DNA channel")
    bl.microstage.move_to(SETUP_PARAMS['name_dna_channel'])
    bl.pause(1.0)
    print("Start DNA catching procedure")
    goto_distance(min_distance, match_threshold, speed=fishing_speed)

    bl.reset_force()
    bl.pause(1.0)

    attempts = 1
    no_dna = True
    print("Starting attempts")
    while no_dna:
        goto_distance(min_distance, match_threshold, speed=fishing_speed)
        bl.reset_force()
        bl.pause(1.0)
        check_distance = max(min_distance, max_distance - 2.0)
        goto_distance(check_distance, speed=fishing_speed)
        no_dna = goto_distance(max_distance, match_threshold, speed=fishing_speed, force_threshold=force_threshold)
        print(f"Fishing for DNA: attempt {attempts}/{max_retries}")

        if attempts >= max_retries:
            raise RuntimeError(f"Max retries {max_retries} reached.")

        attempts += 1

    print("Moving to buffer.")
    stop_flow()
    goto_distance(end_distance, match_threshold)
    bl.microstage.move_to(SETUP_PARAMS['name_buffer_channel'], speed=0.5)
    bl.pause(2.0)


def check_dna(match_threshold, dna_contour_length, distance_threshold, tether_lost_threshold,
              force_check, fclamp_par, end_distance):
    """Check the DNA tether quality by comparing it to the eWLC model at a certain force (given by force_check).
    Throws an error if the fit is bad (not within tolerance given by distance_threshold).

    Parameters
    ----------
    match_threshold : float
        Match threshold for beads (percent); if the match score goes under this threshold, the bead is considered lost.
    dna_contour_length : float
        DNA contour length in microns.
    distance_threshold : float
        Tolerance between bead distance measurement and eWLC predicted DNA length.
    tether_lost_threshold : float
        Force threshold for tether in pN; if the force goes under this value, but the DNA length is larger than
        1.05 times dna_contour_length, the tether is considered lost.
    end_distance : float
        Distance between beads to move to after checking DNA.
    force_check : float
        Force (pN) at which to check the DNA.
    fclamp_par : dict
        Dictionary with force clamp parameters.
    """

    bl.reset_force()
    goto_force(fclamp_par, force_check, match_threshold, tether_lost_threshold, dna_contour_length)

    dna_length_force = DNA_PARAMS['model_ewlc'](force_check, {"model/Lp": DNA_PARAMS['Lp_ds'],
                                                              "model/Lc": DNA_PARAMS['Lc_ds'],
                                                              "model/St": DNA_PARAMS['S'],
                                                              "kT": DNA_PARAMS['kT']})
    dx = dna_length_force - get_distance()
    if abs(dx) > distance_threshold:
        print(dna_length_force, dx, distance_threshold)
        raise RuntimeError("Measured DNA tether length difference with model is too large:" + str(dx) +
                           "um difference with a threshold of" + str(distance_threshold))

    # goto_distance(dna_contour_length*0.6, match_threshold, speed=fishing_speed)
    goto_distance(end_distance, match_threshold)


def validate_dir(path):
    """Make sure the user entered a valid path (str)."""
    path = path if path[-1] == '/' else path + '/'
    if os.path.isdir(path):
        return path
    else:
        raise RuntimeError("Invalid path specified. Did you pass a directory name?")
