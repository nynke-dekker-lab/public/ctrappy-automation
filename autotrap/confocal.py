"""Functions for confocal imaging."""
import numpy as np
import matplotlib.pyplot as plt
import bluelake as bl
import time
import scipy.ndimage
import os
from collections.abc import Iterable


def read_stream(f, t0, t1):
    """Read confocal frame from data stream f between timestamps t0 and t1."""

    # Get data from timeline.
    count_r = f['Photon count']['Red'][t0:t1].data
    count_g = f['Photon count']['Green'][t0:t1].data
    count_b = f['Photon count']['Blue'][t0:t1].data
    n_datapoints = min(len(count_r), len(count_g), len(count_b))
    photon_counts = np.array([count_r[:n_datapoints], count_g[:n_datapoints], count_b[:n_datapoints]])
    info_wave = np.array(f['Info wave']['Info wave'][t0:t1].data, dtype=int)[:n_datapoints]
    frame_clock = np.array(f['Confocal diagnostics']['Frame clock'][t0:t1].data, dtype=int)[:n_datapoints]
    line_clock = np.array(f['Confocal diagnostics']['Line clock'][t0:t1].data, dtype=int)[:n_datapoints]

    # Get flags for start of frames and lines.
    frame_flag = np.concatenate(([0], np.diff(frame_clock)))
    line_flag = np.concatenate(([0], np.diff(line_clock)))
    clock_info = np.array([info_wave, line_flag])

    # And now the final frame, love is a losing game
    i_frame_end = np.argwhere(frame_flag == -1)[-1][0]
    i_frame_start = np.argwhere(frame_flag[:i_frame_end] == 1)[-1][0]

    # Return photon counts and clock info.
    return photon_counts[:, i_frame_start:i_frame_end], clock_info[:, i_frame_start:i_frame_end]
    

def get_image_size(ci):
    """Get image size from clock information array ci."""

    # Calculate the image size by counting lines and pixels.
    image_h = ci[1][ci[1] == 1].sum()
    image_n_px = ci[0][ci[0] == 2].sum() // 2
    image_w = image_n_px // image_h
    return image_w, image_h


def ind_to_xy(i, w):
    """Conversion of flattened pixel index i to 2d pixel indices x, y using the image width w."""
    x = i % w
    y = i // w
    return x, y


def create_image(pc, ci):
    """Function for image reconstruction using photon count array pc and clock information array ci. Returns a
    numpy array."""
    w, h = get_image_size(ci)
    img = np.zeros((h, w, 3), dtype=int)
    i_px = 0
    pixel_vals = np.zeros(3)
    n_timestamps = pc.shape[1]
    for t in range(n_timestamps):
        if ci[0, t] == 1:
            pixel_vals = pixel_vals + pc[:, t]
        elif ci[0, t] == 2:
            pixel_vals = pixel_vals + pc[:, t]
            x, y = ind_to_xy(i_px, w)
            img[y, x] = pixel_vals
            i_px += 1
            pixel_vals = np.zeros(3)
    return img


def get_filtered_projections(im, margin_bead_px):
    """Do image (im) smoothing with a gaussian filter; cut the beads out of the image given by margin_bead_px."""

    gfilter = scipy.ndimage.gaussian_filter1d
    r_filtered = gfilter(np.sum(im[:, :, 0] / im.shape[0], axis=0), 2)[margin_bead_px[0]:-margin_bead_px[1]]
    g_filtered = gfilter(np.sum(im[:, :, 1] / im.shape[0], axis=0), 2)[margin_bead_px[0]:-margin_bead_px[1]]
    b_filtered = gfilter(np.sum(im[:, :, 2] / im.shape[0], axis=0), 2)[margin_bead_px[0]:-margin_bead_px[1]]
    return np.array([r_filtered, g_filtered, b_filtered])


def save_projection_fig(img, filtered_projections, margin_bead_px, thres, fig_path):
    """Save figure containing confocal image and intensity projections."""

    # Confocal scan frame.
    plt.figure()
    plt.imshow(np.clip(img / 3., 0, 1))
    plt.axvline(margin_bead_px[0], c='w', linestyle='dashed')
    plt.axvline(img.shape[1] - margin_bead_px[1], c='w', linestyle='dashed')
    plt.xlabel('x [px]')
    plt.ylabel('y [px]')
    plt.savefig(fig_path + '_frame.png')
    plt.close()

    # Projected intensities.
    plt.figure()
    plt.plot(filtered_projections[0], c='r')
    plt.plot(filtered_projections[1], c='g')
    plt.plot(filtered_projections[2], c='b')
    plt.plot([0, filtered_projections.shape[1]], [thres[0], thres[0]], c='r', linestyle='dashed')
    plt.plot([0, filtered_projections.shape[1]], [thres[1], thres[1]], c='g', linestyle='dashed')
    plt.plot([0, filtered_projections.shape[1]], [thres[2], thres[2]], c='b', linestyle='dashed')
    plt.xlabel('x [px]')
    plt.ylabel('I [ADU]')
    plt.savefig(fig_path + '_intensities.png')
    plt.close()


# Monitor if intensity dips under threshold.
def monitor_intensity(intensity_threshold, image_time, path, experiment_name, margin_bead_px, save_figs):
    """Monitor the fluorescence intensity: return True if the intensity is above intensity_threshold.

    Parameters
    ----------
    intensity_threshold : float
        Intensity threshold (ADU).
    image_time : float
        Frame time in seconds.
    path : str
        Path to save figures to.
    experiment_name : str
        Name of the experiment, for making a filename in case you save the image.
    margin_bead_px : 2-tuple of ints
        Left and right margin to cut out of the image to get rid of the beads.
    save_figs : bool
        If True, save figures.
    """

    if isinstance(intensity_threshold, Iterable):
        thres = intensity_threshold
    else:
        thres = (intensity_threshold, intensity_threshold, intensity_threshold)

    # Give scanner enough time to produce a frame.
    t0 = bl.timeline.current_time
    bl.pause(image_time * 3)
    t1 = bl.timeline.current_time

    # Read stream and generate frame.
    pixels, clock = read_stream(bl.timeline, t0, t1)
    img = create_image(pixels, clock)
    filtered_projections = get_filtered_projections(img, margin_bead_px)

    # Save figures if needed.
    if save_figs:
        fig_path = os.path.join(path, experiment_name)
        save_projection_fig(img, filtered_projections, margin_bead_px, thres, fig_path)

    # Return True if signal above threshold.
    if np.any(filtered_projections[0] >= thres[0]) or \
            np.any(filtered_projections[1] >= thres[1]) or \
            np.any(filtered_projections[2] >= thres[2]):
        return True
    return False


def take_confocal_scan(experiment_name, path, intensity_threshold, image_time=0.6,
                       margin_bead_px=(35, 35), max_t=120., check_interval=5., save_figs=True, preset=None):
    """Take a confocal scan.

    Parameters
    ----------
    experiment_name : str
        Name of the experiment, for making a filename in case you save the image.
    path : str
        Path to save figures to.
    intensity_threshold : float
        Intensity threshold to check if the fluorophores have bleached (ADU).
    image_time : float
        Frame time in seconds.
    margin_bead_px : 2-tuple of ints
        Left and right margin to cut out of the image to get rid of the beads.
    max_t : float
        Maximum length of the scan in seconds.
    check_interval : float
        Check if the fluorophores have bleached every check_interval seconds.
    save_figs : bool
        If True, save figures.
    preset : str
        Use this preset from the list of confocal scan presets in Bluelake. If None, use current settings.
    """

    timestamp = str(int(time.time()))
    try:
        # Start scan.
        bl.timeline.mark_begin(experiment_name + timestamp)
        if preset is None:
            bl.confocal.start_scan()
        else:
            bl.confocal.start_scan(preset_name=preset)
        
        # Scan until bleaching or until max_t
        i = 0
        while i < max_t / (check_interval + 3 * image_time) and \
            monitor_intensity(intensity_threshold, image_time, path,
                              experiment_name + '_' + timestamp + '_' + str(i).zfill(3),
                              margin_bead_px, save_figs):
            i = i + 1
            bl.pause(check_interval)

    except RuntimeError as e:
        print(e)
        print("Confocal scan failed.")

    # Finish and store scan.
    finally:
        bl.confocal.abort_scan()
        bl.timeline.mark_end(export=True if path else False,
                             filepath=f"{path}/{experiment_name}.h5")


def check_fluorescence_frame(experiment_name, path, intensity_threshold,
                             margin_bead_px=(35, 35), preset_single_frame='Single frame'):
    """Take a single confocal frame to check if there is any signal.

    Parameters
    ----------
    experiment_name : str
        Name of the experiment, for making a filename in case you save the image.
    path : str
        Path to save figures to.
    intensity_threshold : float
        Intensity threshold to check if there are fluorophores on the DNA (ADU).
    margin_bead_px : 2-tuple of ints
        Left and right margin to cut out of the image to get rid of the beads.
    preset_single_frame : str
        Use this single frame preset from the list of confocal scan presets in Bluelake.
    """

    if isinstance(intensity_threshold, Iterable):
        thres = intensity_threshold
    else:
        thres = (intensity_threshold, intensity_threshold, intensity_threshold)

    # Save scan scan.
    timestamp = str(int(time.time()))
    try:
        # Start scan.
        t0 = bl.timeline.current_time
        bl.pause(0.5)
        bl.timeline.mark_begin(experiment_name + timestamp)
        bl.confocal.start_scan(preset_name=preset_single_frame)
        bl.confocal.wait()
        bl.pause(0.5)
        t1 = bl.timeline.current_time
    except RuntimeError as e:
        print(e)
        print("Fluorescence check failed.")
        return False
    finally:
        bl.confocal.abort_scan()
        bl.timeline.mark_end(export=True if path else False,
                             filepath=f"{path}/{experiment_name}_fluorescence_check.h5")

    # Read stream and generate frame.
    pixels, clock = read_stream(bl.timeline, t0, t1)
    img = create_image(pixels, clock)
    filtered_projections = get_filtered_projections(img, margin_bead_px)

    # Return True if signal above threshold.
    if np.any(filtered_projections[0] >= thres[0]) or \
            np.any(filtered_projections[1] >= thres[1]) or \
            np.any(filtered_projections[2] >= thres[2]):
        return True
    return False
